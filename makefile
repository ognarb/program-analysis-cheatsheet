BUILDDIR := public
TEX      := $(sort $(wildcard *.tex))
PDF      := $(TEX:%.tex=%)

all: builddir $(PDF)

builddir:
	mkdir -p $(BUILDDIR)

%: %.tex
	latexmk -halt-on-error -pdf -jobname=$@ $<

clean:
	rm -rf $(BUILDDIR)
